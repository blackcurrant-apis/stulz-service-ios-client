#
# Be sure to run `pod lib lint SwaggerClient.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "SwaggerClient"
    s.version          = "1.0.2"

    s.summary          = "Stulz Api"
    s.description      = <<-DESC
                         Apis to access stulz backend services
                         DESC

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.framework    = 'SystemConfiguration'

	s.homepage      = "http://www.blackcurrantapps.com"
    s.license      = "Proprietary"
    s.source       = { :git => "https://gitlab.com/blackcurrant-apis/stulz-service-ios-client.git" }
    s.author       = { "Sanket" => "sanket@blackcurrantapps.com" }

    s.source_files = 'SwaggerClient/**/*.{m,h}'
    s.public_header_files = 'SwaggerClient/**/*.h'


    s.dependency 'AFNetworking', '~> 3'
    s.dependency 'JSONModel', '~> 1.2'
    s.dependency 'ISO8601', '~> 0.6'
end

