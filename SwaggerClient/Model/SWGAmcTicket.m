#import "SWGAmcTicket.h"

@implementation SWGAmcTicket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"amcTicketId": @"amc_ticket_id", @"serviceDate": @"service_date", @"plantCode": @"plant_code", @"createdBy": @"created_by", @"createdByName": @"created_by_name", @"createdByType": @"created_by_type", @"clientLatitude": @"client_latitude", @"clientLongitude": @"client_longitude", @"clientAddress": @"client_address", @"ticketWbsDesc": @"ticket_wbs_desc", @"status": @"status", @"clientId": @"client_id", @"warrantyType": @"warranty_type", @"updatedBy": @"updated_by", @"updatedByName": @"updated_by_name", @"updatedByType": @"updated_by_type", @"equipments": @"equipments", @"customerId": @"customer_id", @"clientName": @"client_name", @"contractNumber": @"contract_number", @"severity": @"severity", @"_description": @"description", @"branchManagerId": @"branch_manager_id", @"tsFirstManagerAssigned": @"ts_first_manager_assigned", @"tsFirstTechnicianAssigned": @"ts_first_technician_assigned", @"tsPriorityUpdate": @"ts_priority_update", @"tsFirstTechnicianArrived": @"ts_first_technician_arrived", @"managerIsApproved": @"manager_is_approved", @"managerRemark": @"manager_remark", @"clientIsApproved": @"client_is_approved", @"clientRating": @"client_rating", @"totalTimeSpentOnsite": @"total_time_spent_onsite", @"timeOverhead": @"time_overhead", @"createdAt": @"created_at", @"updatedAt": @"updated_at" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"serviceDate", @"ticketWbsDesc", @"warrantyType", @"updatedBy", @"updatedByName", @"updatedByType", @"customerId", @"contractNumber", @"severity", @"_description", @"branchManagerId", @"tsFirstManagerAssigned", @"tsFirstTechnicianAssigned", @"tsPriorityUpdate", @"tsFirstTechnicianArrived", @"managerIsApproved", @"managerRemark", @"clientIsApproved", @"clientRating", @"totalTimeSpentOnsite", @"timeOverhead", @"updatedAt"];
  return [optionalProperties containsObject:propertyName];
}

@end
