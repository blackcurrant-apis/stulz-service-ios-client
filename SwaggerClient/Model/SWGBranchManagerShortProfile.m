#import "SWGBranchManagerShortProfile.h"

@implementation SWGBranchManagerShortProfile

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"branchManagerId": @"branch_manager_id", @"branchManagerUserId": @"branch_manager_user_id", @"branchManagerEmailId": @"branch_manager_email_id", @"branchManagerName": @"branch_manager_name", @"branchManagerPlantCode": @"branch_manager_plant_code", @"branchManagerPhone": @"branch_manager_phone", @"branchManagerProfilePicUrl": @"branch_manager_profile_pic_url" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"branchManagerEmailId", @"branchManagerProfilePicUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
