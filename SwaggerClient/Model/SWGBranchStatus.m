#import "SWGBranchStatus.h"

@implementation SWGBranchStatus

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"ticketsCreated": @"tickets_created", @"amcTicketsCreated": @"amc_tickets_created", @"ticketsInProgress": @"tickets_in_progress", @"amcTicketsInProgress": @"amc_tickets_in_progress", @"ticketsOnHold": @"tickets_on_hold", @"amcTicketsOnHold": @"amc_tickets_on_hold", @"ticketsClosed": @"tickets_closed", @"amcTicketsClosed": @"amc_tickets_closed", @"ticketsResolved": @"tickets_resolved", @"amcTicketsResolved": @"amc_tickets_resolved", @"ticketsUrgent": @"tickets_urgent", @"ticketsOverdue": @"tickets_overdue", @"ticketsServicedThisMonth": @"tickets_serviced_this_month", @"amcTicketsServicedThisMonth": @"amc_tickets_serviced_this_month", @"pendingSpareApprovals": @"pending_spare_approvals", @"pendingOvertimeApprovals": @"pending_overtime_approvals", @"techniciansTotal": @"technicians_total", @"techniciansAvailableToday": @"technicians_available_today" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"ticketsCreated", @"amcTicketsCreated", @"ticketsInProgress", @"amcTicketsInProgress", @"ticketsOnHold", @"amcTicketsOnHold", @"ticketsClosed", @"amcTicketsClosed", @"ticketsResolved", @"amcTicketsResolved", @"ticketsUrgent", @"ticketsOverdue", @"ticketsServicedThisMonth", @"amcTicketsServicedThisMonth", @"pendingSpareApprovals", @"pendingOvertimeApprovals", @"techniciansTotal", @"techniciansAvailableToday"];
  return [optionalProperties containsObject:propertyName];
}

@end
