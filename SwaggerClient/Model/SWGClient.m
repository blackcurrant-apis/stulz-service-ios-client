#import "SWGClient.h"

@implementation SWGClient

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"clientId": @"client_id", @"clientName": @"client_name", @"postalCode": @"postal_code", @"clientPlantCode": @"client_plant_code", @"clientEmail": @"client_email", @"clientPhone": @"client_phone", @"clientCity": @"client_city", @"clientLatitude": @"client_latitude", @"clientLongitude": @"client_longitude", @"wbsDesc": @"wbs_desc", @"createdAt": @"created_at", @"updatedAt": @"updated_at", @"createdBy": @"created_by", @"updatedBy": @"updated_by", @"canAccess": @"can_access" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"postalCode", @"clientEmail", @"clientPhone", @"clientCity", @"wbsDesc", @"createdAt", @"updatedAt", @"createdBy", @"updatedBy", @"canAccess"];
  return [optionalProperties containsObject:propertyName];
}

@end
