#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Stulz Api
* Apis to access stulz backend services
*
* OpenAPI spec version: 0.0.1
* Contact: sanket@blackcurrantapps.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGInlineResponse200
@end

@interface SWGInlineResponse200 : SWGObject

/* current billing status [optional]
 */
@property(nonatomic) NSString* billingStatus;
/* days since payment is due. 0 if paid. [optional]
 */
@property(nonatomic) NSNumber* elapsedDays;
/* date when last payment was made [optional]
 */
@property(nonatomic) NSString* lastPaidDate;

@end
