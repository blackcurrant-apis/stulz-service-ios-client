#import "SWGOvertimeRequest.h"

@implementation SWGOvertimeRequest

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.wasInGeofence = @(NO);
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"technicianId": @"technician_id", @"technicianProfile": @"technician_profile", @"overtimeStartTime": @"overtime_start_time", @"overtimeEndTime": @"overtime_end_time", @"overtimeRequestedHours": @"overtime_requested_hours", @"overtimeApprovedHours": @"overtime_approved_hours", @"overtimeRequestId": @"overtime_request_id", @"overtimeComment": @"overtime_comment", @"overtimeRequestLat": @"overtime_request_lat", @"overtimeRequestLon": @"overtime_request_lon", @"overtimeRequestTimestamp": @"overtime_request_timestamp", @"ticketType": @"ticket_type", @"ticketId": @"ticket_id", @"requestStatus": @"request_status", @"wasInGeofence": @"was_in_geofence", @"distanceFromClient": @"distance_from_client" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"technicianId", @"technicianProfile", @"overtimeStartTime", @"overtimeEndTime", @"overtimeRequestedHours", @"overtimeApprovedHours", @"overtimeRequestId", @"overtimeComment", @"overtimeRequestLat", @"overtimeRequestLon", @"overtimeRequestTimestamp", @"ticketType", @"ticketId", @"requestStatus", @"wasInGeofence", @"distanceFromClient"];
  return [optionalProperties containsObject:propertyName];
}

@end
