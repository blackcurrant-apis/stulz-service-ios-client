#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* Stulz Api
* Apis to access stulz backend services
*
* OpenAPI spec version: 0.0.1
* Contact: sanket@blackcurrantapps.com
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGReport
@end

@interface SWGReport : SWGObject

/* Unique identifier for the report [optional]
 */
@property(nonatomic) NSString* reportId;

@property(nonatomic) NSString* _description;
/* the uploaded file [optional]
 */
@property(nonatomic) NSString* fileUrl;

@end
