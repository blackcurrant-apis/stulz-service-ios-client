#import "SWGSpareMaterial.h"

@implementation SWGSpareMaterial

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"materialId": @"material_id", @"materialDescription": @"material_description", @"materialBillingUnit": @"material_billing_unit", @"materialType": @"material_type", @"materialGroup": @"material_group" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"materialId", @"materialDescription", @"materialBillingUnit", @"materialType", @"materialGroup"];
  return [optionalProperties containsObject:propertyName];
}

@end
