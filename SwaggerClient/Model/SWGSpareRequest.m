#import "SWGSpareRequest.h"

@implementation SWGSpareRequest

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"requestTicketId": @"request_ticket_id", @"requestClientName": @"request_client_name", @"requestEquipmentId": @"request_equipment_id", @"requestId": @"request_id", @"oldSerialNo": @"old_serial_no", @"varNewSerialNo": @"new_serial_no", @"material": @"material", @"requestStatus": @"request_status", @"requestComment": @"request_comment", @"approvedQuantity": @"approved_quantity" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"requestTicketId", @"requestClientName", @"requestEquipmentId", @"requestId", @"oldSerialNo", @"varNewSerialNo", @"material", @"requestStatus", @"requestComment", @"approvedQuantity"];
  return [optionalProperties containsObject:propertyName];
}

@end
