#import "SWGTechnician.h"

@implementation SWGTechnician

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"technicianId": @"technician_id", @"technicianUserId": @"technician_user_id", @"technicianName": @"technician_name", @"technicianRegionCode": @"technician_region_code", @"technicianPhone": @"technician_phone", @"createdAt": @"created_at", @"createdBy": @"created_by", @"technicianAddress": @"technician_address", @"technicianPostCode": @"technician_post_code", @"updatedAt": @"updated_at", @"updatedBy": @"updated_by", @"technicianProfilePicUrl": @"technician_profile_pic_url", @"technicianWorkingDays": @"technician_working_days", @"technicianStartTime": @"technician_start_time", @"technicianPlantCodes": @"technician_plant_codes", @"technicianLastLogin": @"technician_last_login", @"technicianIsAvailableToday": @"technician_is_available_today", @"technicianActiveBreakdownTickets": @"technician_active_breakdown_tickets", @"technicianActiveAmcTickets": @"technician_active_amc_tickets", @"technicianTicketsWorkingOnToday": @"technician_tickets_working_on_today", @"loginTimestamp": @"login_timestamp", @"loginLat": @"login_lat", @"loginLon": @"login_lon", @"logoutTimestamp": @"logout_timestamp", @"logoutLat": @"logout_lat", @"logoutLon": @"logout_lon" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"technicianRegionCode", @"technicianAddress", @"technicianPostCode", @"updatedAt", @"updatedBy", @"technicianProfilePicUrl", @"technicianPlantCodes", @"technicianLastLogin", @"technicianIsAvailableToday", @"technicianActiveBreakdownTickets", @"technicianActiveAmcTickets", @"technicianTicketsWorkingOnToday", @"loginTimestamp", @"loginLat", @"loginLon", @"logoutTimestamp", @"logoutLat", @"logoutLon"];
  return [optionalProperties containsObject:propertyName];
}

@end
