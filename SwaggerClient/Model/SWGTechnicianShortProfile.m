#import "SWGTechnicianShortProfile.h"

@implementation SWGTechnicianShortProfile

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"technicianId": @"technician_id", @"technicianUserId": @"technician_user_id", @"technicianName": @"technician_name", @"technicianRegionCode": @"technician_region_code", @"technicianPhone": @"technician_phone", @"technicianAddress": @"technician_address", @"technicianPostCode": @"technician_post_code", @"technicianProfilePicUrl": @"technician_profile_pic_url" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"technicianRegionCode", @"technicianAddress", @"technicianPostCode", @"technicianProfilePicUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
