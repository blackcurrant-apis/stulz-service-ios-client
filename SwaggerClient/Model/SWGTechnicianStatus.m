#import "SWGTechnicianStatus.h"

@implementation SWGTechnicianStatus

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"ticketsInProgress": @"tickets_in_progress", @"amcTicketsNotResolved": @"amc_tickets_not_resolved", @"ticketsOnHold": @"tickets_on_hold", @"ticketsResolved": @"tickets_resolved", @"ticketsUrgent": @"tickets_urgent", @"ticketsOverdue": @"tickets_overdue", @"pendingSpareApprovals": @"pending_spare_approvals", @"pendingOvertimeApprovals": @"pending_overtime_approvals", @"ticketsAssignedToday": @"tickets_assigned_today" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"ticketsInProgress", @"amcTicketsNotResolved", @"ticketsOnHold", @"ticketsResolved", @"ticketsUrgent", @"ticketsOverdue", @"pendingSpareApprovals", @"pendingOvertimeApprovals", @"ticketsAssignedToday"];
  return [optionalProperties containsObject:propertyName];
}

@end
