#import "SWGTicket.h"

@implementation SWGTicket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"ticketId": @"ticket_id", @"plantCode": @"plant_code", @"createdBy": @"created_by", @"createdByName": @"created_by_name", @"createdByType": @"created_by_type", @"clientLatitude": @"client_latitude", @"clientLongitude": @"client_longitude", @"clientAddress": @"client_address", @"ticketWbsDesc": @"ticket_wbs_desc", @"status": @"status", @"priority": @"priority", @"clientId": @"client_id", @"updatedBy": @"updated_by", @"updatedByName": @"updated_by_name", @"updatedByType": @"updated_by_type", @"equipmentId": @"equipment_id", @"modelNumber": @"model_number", @"serialNumber": @"serial_number", @"customerId": @"customer_id", @"clientName": @"client_name", @"warrantyType": @"warranty_type", @"contractNumber": @"contract_number", @"severity": @"severity", @"callingPerson": @"calling_person", @"callingPersonPhone": @"calling_person_phone", @"callingPersonEmail": @"calling_person_email", @"_description": @"description", @"branchManagerId": @"branch_manager_id", @"tsFirstManagerAssigned": @"ts_first_manager_assigned", @"tsFirstTechnicianAssigned": @"ts_first_technician_assigned", @"tsPriorityUpdate": @"ts_priority_update", @"tsFirstTechnicianArrived": @"ts_first_technician_arrived", @"managerIsApproved": @"manager_is_approved", @"managerRemark": @"manager_remark", @"clientIsApproved": @"client_is_approved", @"clientRating": @"client_rating", @"totalTimeSpentOnsite": @"total_time_spent_onsite", @"timeOverhead": @"time_overhead", @"createdAt": @"created_at", @"updatedAt": @"updated_at", @"originalFaultCode": @"original_fault_code", @"technicalFaultCode": @"technical_fault_code" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"ticketWbsDesc", @"updatedBy", @"updatedByName", @"updatedByType", @"equipmentId", @"modelNumber", @"serialNumber", @"customerId", @"warrantyType", @"contractNumber", @"severity", @"callingPerson", @"callingPersonPhone", @"callingPersonEmail", @"_description", @"branchManagerId", @"tsFirstManagerAssigned", @"tsFirstTechnicianAssigned", @"tsPriorityUpdate", @"tsFirstTechnicianArrived", @"managerIsApproved", @"managerRemark", @"clientIsApproved", @"clientRating", @"totalTimeSpentOnsite", @"timeOverhead", @"updatedAt", @"originalFaultCode", @"technicalFaultCode"];
  return [optionalProperties containsObject:propertyName];
}

@end
