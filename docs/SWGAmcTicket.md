# SWGAmcTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amcTicketId** | **NSNumber*** | UID of AMC ticket | 
**serviceDate** | **NSString*** | Day on which the servicing is scheduled | [optional] 
**plantCode** | **NSString*** | UID of plant to which services this ticket | 
**createdBy** | **NSNumber*** | The UID of the creator of this ticket | 
**createdByName** | **NSString*** | Name of the creator of the ticket | 
**createdByType** | **NSString*** | The user type who created the ticket | 
**clientLatitude** | **NSNumber*** | The location latitude where the ticket is to be serviced | 
**clientLongitude** | **NSNumber*** | The location longitude where the ticket is to be serviced | 
**clientAddress** | **NSString*** | physical access of this ticket | 
**ticketWbsDesc** | **NSString*** | floor number and location of the unit | [optional] 
**status** | **NSString*** | All possible statuses of tickets | 
**clientId** | **NSString*** | Client ID eg. Infosis Branch, Seepz | 
**warrantyType** | **NSString*** | the Warranty Type of the equipment. | [optional] 
**updatedBy** | **NSNumber*** | UID of user who last updated this ticket | [optional] 
**updatedByName** | **NSString*** | Name of the creator of the ticket | [optional] 
**updatedByType** | **NSString*** | The user type who updated the ticket | [optional] 
**equipments** | [**NSArray&lt;SWGAmcTicketEquipments&gt;***](SWGAmcTicketEquipments.md) | The equipments under amc | 
**customerId** | **NSString*** | High level customer ID eg. Infosis India | [optional] 
**clientName** | **NSString*** | Client Name | 
**contractNumber** | **NSString*** | UID Of contract. Contract &lt;--&gt; Customer | [optional] 
**severity** | **NSString*** | Enum (Ex. Blocker) | [optional] 
**_description** | **NSString*** | Description entered by help desk or technician | [optional] 
**branchManagerId** | **NSNumber*** | ID Of branch manager who assigned the ticket | [optional] 
**tsFirstManagerAssigned** | **NSString*** | Timestamp when first manager was assigned | [optional] 
**tsFirstTechnicianAssigned** | **NSString*** | Timestamp when first technician was assigned | [optional] 
**tsPriorityUpdate** | **NSString*** | Timestamp when priority was last updated | [optional] 
**tsFirstTechnicianArrived** | **NSString*** | Timestamp when first technician arrived on Client Site within the GEOFence | [optional] 
**managerIsApproved** | **NSNumber*** | Is the completion approved by manager | [optional] 
**managerRemark** | **NSString*** | optional manager remark | [optional] 
**clientIsApproved** | **NSNumber*** | is the completion approved by client | [optional] 
**clientRating** | **NSNumber*** | 1 to 5 stars | [optional] 
**totalTimeSpentOnsite** | **NSNumber*** | Time Spent onsite in Minutes. | [optional] 
**timeOverhead** | **NSNumber*** | Total overtime spent on this ticket by various technicians in minutes | [optional] 
**createdAt** | **NSString*** | Timestamp when the ticket was created at | 
**updatedAt** | **NSString*** | Timestamp when the ticket was last updated | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


