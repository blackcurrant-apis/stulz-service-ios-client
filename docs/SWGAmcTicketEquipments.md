# SWGAmcTicketEquipments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**equipmentId** | **NSString*** |  | [optional] 
**modelNo** | **NSString*** |  | [optional] 
**serialNo** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


