# SWGAppVersionInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latestVersionName** | **NSString*** |  | [optional] 
**latestVersionCode** | **NSNumber*** |  | [optional] 
**lastSupportedVersionCode** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


