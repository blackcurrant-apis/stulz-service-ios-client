# SWGAuthenticationApi

All URIs are relative to *http://stulzdemo.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticateBranchManagerPost**](SWGAuthenticationApi.md#authenticatebranchmanagerpost) | **POST** /authenticate/branch_manager | 
[**authenticateTechnicianPost**](SWGAuthenticationApi.md#authenticatetechnicianpost) | **POST** /authenticate/technician | 


# **authenticateBranchManagerPost**
```objc
-(NSURLSessionTask*) authenticateBranchManagerPostWithEmail: (NSString*) email
    password: (NSString*) password
        completionHandler: (void (^)(SWGToken* output, NSError* error)) handler;
```



Get Token after authentication

### Example 
```objc

NSString* email = @"email_example"; // Email ID
NSString* password = @"password_example"; // Password

SWGAuthenticationApi*apiInstance = [[SWGAuthenticationApi alloc] init];

[apiInstance authenticateBranchManagerPostWithEmail:email
              password:password
          completionHandler: ^(SWGToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAuthenticationApi->authenticateBranchManagerPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **NSString***| Email ID | 
 **password** | **NSString***| Password | 

### Return type

[**SWGToken***](SWGToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authenticateTechnicianPost**
```objc
-(NSURLSessionTask*) authenticateTechnicianPostWithPhone: (NSString*) phone
    pin: (NSString*) pin
        completionHandler: (void (^)(SWGToken* output, NSError* error)) handler;
```



Get Token after authentication

### Example 
```objc

NSString* phone = @"phone_example"; // Email ID
NSString* pin = @"pin_example"; // Password

SWGAuthenticationApi*apiInstance = [[SWGAuthenticationApi alloc] init];

[apiInstance authenticateTechnicianPostWithPhone:phone
              pin:pin
          completionHandler: ^(SWGToken* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAuthenticationApi->authenticateTechnicianPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***| Email ID | 
 **pin** | **NSString***| Password | 

### Return type

[**SWGToken***](SWGToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

