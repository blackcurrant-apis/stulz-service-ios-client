# SWGBranchManagerApi

All URIs are relative to *http://stulzdemo.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bmAddTicketCommentPost**](SWGBranchManagerApi.md#bmaddticketcommentpost) | **POST** /bm/add_ticket_comment | 
[**bmAmcUpgradeToBreakdownPost**](SWGBranchManagerApi.md#bmamcupgradetobreakdownpost) | **POST** /bm/amc_upgrade_to_breakdown | 
[**bmApproveOvertimeRequestPost**](SWGBranchManagerApi.md#bmapproveovertimerequestpost) | **POST** /bm/approve_overtime_request | 
[**bmApproveSparesRequestPost**](SWGBranchManagerApi.md#bmapprovesparesrequestpost) | **POST** /bm/approve_spares_request | 
[**bmAssignTechnicianPost**](SWGBranchManagerApi.md#bmassigntechnicianpost) | **POST** /bm/assign_technician | 
[**bmCreateBreakdownTicketPost**](SWGBranchManagerApi.md#bmcreatebreakdownticketpost) | **POST** /bm/create_breakdown_ticket | 
[**bmDenyOvertimeRequestPost**](SWGBranchManagerApi.md#bmdenyovertimerequestpost) | **POST** /bm/deny_overtime_request | 
[**bmDenySparesRequestPost**](SWGBranchManagerApi.md#bmdenysparesrequestpost) | **POST** /bm/deny_spares_request | 
[**bmForceAllLocationUpdatePost**](SWGBranchManagerApi.md#bmforcealllocationupdatepost) | **POST** /bm/force_all_location_update | 
[**bmForceLocationUpdatePost**](SWGBranchManagerApi.md#bmforcelocationupdatepost) | **POST** /bm/force_location_update | 
[**bmGetAmcTicketAmcTicketIdGet**](SWGBranchManagerApi.md#bmgetamcticketamcticketidget) | **GET** /bm/get_amc_ticket/{amc_ticket_id} | 
[**bmGetAmcTicketsGet**](SWGBranchManagerApi.md#bmgetamcticketsget) | **GET** /bm/get_amc_tickets | 
[**bmGetAmcTypeClientIdGet**](SWGBranchManagerApi.md#bmgetamctypeclientidget) | **GET** /bm/get_amc_type/{client_id} | 
[**bmGetAppVersionGet**](SWGBranchManagerApi.md#bmgetappversionget) | **GET** /bm/get_app_version | 
[**bmGetBillingStatusClientIdGet**](SWGBranchManagerApi.md#bmgetbillingstatusclientidget) | **GET** /bm/get_billing_status/{client_id} | 
[**bmGetBranchStatusGet**](SWGBranchManagerApi.md#bmgetbranchstatusget) | **GET** /bm/get_branch_status | 
[**bmGetBreakdownTicketTicketIdGet**](SWGBranchManagerApi.md#bmgetbreakdownticketticketidget) | **GET** /bm/get_breakdown_ticket/{ticket_id} | 
[**bmGetBreakdownTicketsGet**](SWGBranchManagerApi.md#bmgetbreakdownticketsget) | **GET** /bm/get_breakdown_tickets | 
[**bmGetOverdueAmcTicketsGet**](SWGBranchManagerApi.md#bmgetoverdueamcticketsget) | **GET** /bm/get_overdue_amc_tickets | 
[**bmGetOverdueBreakdownTicketsGet**](SWGBranchManagerApi.md#bmgetoverduebreakdownticketsget) | **GET** /bm/get_overdue_breakdown_tickets | 
[**bmGetOvertimeRequestsGet**](SWGBranchManagerApi.md#bmgetovertimerequestsget) | **GET** /bm/get_overtime_requests | 
[**bmGetPendingSpareRequestsGet**](SWGBranchManagerApi.md#bmgetpendingsparerequestsget) | **GET** /bm/get_pending_spare_requests | 
[**bmGetProfileGet**](SWGBranchManagerApi.md#bmgetprofileget) | **GET** /bm/get_profile | 
[**bmGetTechnicalFaultCodeGroupItemsGet**](SWGBranchManagerApi.md#bmgettechnicalfaultcodegroupitemsget) | **GET** /bm/get_technical_fault_code_group_items | 
[**bmGetTechnicalFaultCodeGroupsGet**](SWGBranchManagerApi.md#bmgettechnicalfaultcodegroupsget) | **GET** /bm/get_technical_fault_code_groups | 
[**bmGetTechnicalFaultCodesGet**](SWGBranchManagerApi.md#bmgettechnicalfaultcodesget) | **GET** /bm/get_technical_fault_codes | 
[**bmGetTechnicianTechnicianIdGet**](SWGBranchManagerApi.md#bmgettechniciantechnicianidget) | **GET** /bm/get_technician/{technician_id} | 
[**bmGetTicketsThisMonthGet**](SWGBranchManagerApi.md#bmgetticketsthismonthget) | **GET** /bm/get_tickets_this_month | 
[**bmGetUrgentTicketsGet**](SWGBranchManagerApi.md#bmgeturgentticketsget) | **GET** /bm/get_urgent_tickets | 
[**bmGetUserFacingFaultCodeGroupItemsGet**](SWGBranchManagerApi.md#bmgetuserfacingfaultcodegroupitemsget) | **GET** /bm/get_user_facing_fault_code_group_items | 
[**bmGetUserFacingFaultCodeGroupsGet**](SWGBranchManagerApi.md#bmgetuserfacingfaultcodegroupsget) | **GET** /bm/get_user_facing_fault_code_groups | 
[**bmGetUserFacingFaultCodesGet**](SWGBranchManagerApi.md#bmgetuserfacingfaultcodesget) | **GET** /bm/get_user_facing_fault_codes | 
[**bmListAvailableTechniciansGet**](SWGBranchManagerApi.md#bmlistavailabletechniciansget) | **GET** /bm/list_available_technicians | 
[**bmListTechniciansGet**](SWGBranchManagerApi.md#bmlisttechniciansget) | **GET** /bm/list_technicians | 
[**bmMarkTechnicianTodayTicketsPost**](SWGBranchManagerApi.md#bmmarktechniciantodayticketspost) | **POST** /bm/mark_technician_today_tickets | 
[**bmNotifyChatMessagePost**](SWGBranchManagerApi.md#bmnotifychatmessagepost) | **POST** /bm/notify_chat_message | 
[**bmQueryClientsNonDistinctGet**](SWGBranchManagerApi.md#bmqueryclientsnondistinctget) | **GET** /bm/query_clients_non_distinct | 
[**bmQueryDistinctClientCityGet**](SWGBranchManagerApi.md#bmquerydistinctclientcityget) | **GET** /bm/query_distinct_client_city | 
[**bmQueryDistinctClientNameGet**](SWGBranchManagerApi.md#bmquerydistinctclientnameget) | **GET** /bm/query_distinct_client_name | 
[**bmUnAssignTechnicianPost**](SWGBranchManagerApi.md#bmunassigntechnicianpost) | **POST** /bm/un_assign_technician | 
[**bmUpdateFaultCodePost**](SWGBranchManagerApi.md#bmupdatefaultcodepost) | **POST** /bm/update_fault_code | 
[**bmUpdateGcmRegIdPost**](SWGBranchManagerApi.md#bmupdategcmregidpost) | **POST** /bm/update_gcm_reg_id | 
[**bmUpdateProfilePicturePost**](SWGBranchManagerApi.md#bmupdateprofilepicturepost) | **POST** /bm/update_profile_picture | 
[**bmUpdateTicketStatusPost**](SWGBranchManagerApi.md#bmupdateticketstatuspost) | **POST** /bm/update_ticket_status | 
[**bmUploadReportPost**](SWGBranchManagerApi.md#bmuploadreportpost) | **POST** /bm/upload_report | 


# **bmAddTicketCommentPost**
```objc
-(NSURLSessionTask*) bmAddTicketCommentPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    comment: (NSString*) comment
        completionHandler: (void (^)(NSError* error)) handler;
```



add comment to ticket

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // UID of the `Ticket`
NSString* comment = @"comment_example"; // comment to add

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmAddTicketCommentPostWithTicketType:ticketType
              ticketId:ticketId
              comment:comment
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmAddTicketCommentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| UID of the &#x60;Ticket&#x60; | 
 **comment** | **NSString***| comment to add | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmAmcUpgradeToBreakdownPost**
```objc
-(NSURLSessionTask*) bmAmcUpgradeToBreakdownPostWithAmcTicketId: (NSNumber*) amcTicketId
    equipmentId: (NSString*) equipmentId
    comment: (NSString*) comment
        completionHandler: (void (^)(NSError* error)) handler;
```



Upgrade an amc ticket to breakdown

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* amcTicketId = @789; // the `Ticket` id of the Ticket in Vicinity
NSString* equipmentId = @"equipmentId_example"; // the `Equipment ID` id of the AMC Ticket for which a breakdown ticket needs to be genrated.
NSString* comment = @"comment_example"; // 

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmAmcUpgradeToBreakdownPostWithAmcTicketId:amcTicketId
              equipmentId:equipmentId
              comment:comment
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmAmcUpgradeToBreakdownPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amcTicketId** | **NSNumber***| the &#x60;Ticket&#x60; id of the Ticket in Vicinity | 
 **equipmentId** | **NSString***| the &#x60;Equipment ID&#x60; id of the AMC Ticket for which a breakdown ticket needs to be genrated. | 
 **comment** | **NSString***|  | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmApproveOvertimeRequestPost**
```objc
-(NSURLSessionTask*) bmApproveOvertimeRequestPostWithRequestId: (NSNumber*) requestId
    hoursApproved: (NSNumber*) hoursApproved
        completionHandler: (void (^)(NSError* error)) handler;
```



Approve a spare request

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* requestId = @789; // The Request ID to approve
NSNumber* hoursApproved = @3.4; // the number of hours approved.

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmApproveOvertimeRequestPostWithRequestId:requestId
              hoursApproved:hoursApproved
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmApproveOvertimeRequestPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **NSNumber***| The Request ID to approve | 
 **hoursApproved** | **NSNumber***| the number of hours approved. | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmApproveSparesRequestPost**
```objc
-(NSURLSessionTask*) bmApproveSparesRequestPostWithRequestId: (NSString*) requestId
    isReturnable: (NSNumber*) isReturnable
    isBillable: (NSNumber*) isBillable
    varNewSerialNo: (NSString*) varNewSerialNo
    approveQuantity: (NSNumber*) approveQuantity
        completionHandler: (void (^)(NSError* error)) handler;
```



Approve the spare request and choose wheteher the old part is returnable

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* requestId = @"requestId_example"; // the request id to approve
NSNumber* isReturnable = @true; // wheteher or not a return request needs to be generated
NSNumber* isBillable = @true; // wheteher or not a this item is billable
NSString* varNewSerialNo = @"varNewSerialNo_example"; // Serial number of the new part which will be dispatched. (optional)
NSNumber* approveQuantity = @1; // No/Kg of items to be approved (optional) (default to 1)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmApproveSparesRequestPostWithRequestId:requestId
              isReturnable:isReturnable
              isBillable:isBillable
              varNewSerialNo:varNewSerialNo
              approveQuantity:approveQuantity
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmApproveSparesRequestPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **NSString***| the request id to approve | 
 **isReturnable** | **NSNumber***| wheteher or not a return request needs to be generated | 
 **isBillable** | **NSNumber***| wheteher or not a this item is billable | 
 **varNewSerialNo** | **NSString***| Serial number of the new part which will be dispatched. | [optional] 
 **approveQuantity** | **NSNumber***| No/Kg of items to be approved | [optional] [default to 1]

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmAssignTechnicianPost**
```objc
-(NSURLSessionTask*) bmAssignTechnicianPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    technicianIds: (NSArray<NSNumber*>*) technicianIds
        completionHandler: (void (^)(NSError* error)) handler;
```



Assign a `Technician` to a `Ticket`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // The `ticketId` of the ticket to assign
NSArray<NSNumber*>* technicianIds = @[@56]; // UIDs of the technician to assign

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmAssignTechnicianPostWithTicketType:ticketType
              ticketId:ticketId
              technicianIds:technicianIds
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmAssignTechnicianPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| The &#x60;ticketId&#x60; of the ticket to assign | 
 **technicianIds** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| UIDs of the technician to assign | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmCreateBreakdownTicketPost**
```objc
-(NSURLSessionTask*) bmCreateBreakdownTicketPostWithClientId: (NSString*) clientId
    equipmentId: (NSString*) equipmentId
    priority: (NSString*) priority
    faultCode: (NSString*) faultCode
    _description: (NSString*) _description
    callingPersonName: (NSString*) callingPersonName
    callingPersonPhone: (NSString*) callingPersonPhone
    callingPersonEmail: (NSString*) callingPersonEmail
        completionHandler: (void (^)(SWGTicket* output, NSError* error)) handler;
```



Create a new `Ticket`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* clientId = @"clientId_example"; // Client ID
NSString* equipmentId = @"equipmentId_example"; // Optional UID Of equipment (optional)
NSString* priority = @"priority_example"; // Optional Priority (optional)
NSString* faultCode = @"faultCode_example"; // Fault Code (optional)
NSString* _description = @"_description_example"; // Optional ticket description or comments (optional)
NSString* callingPersonName = @"callingPersonName_example"; // Optional calling person name (optional)
NSString* callingPersonPhone = @"callingPersonPhone_example"; // Optional calling person phone number (optional)
NSString* callingPersonEmail = @"callingPersonEmail_example"; // Optional calling person email_id (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmCreateBreakdownTicketPostWithClientId:clientId
              equipmentId:equipmentId
              priority:priority
              faultCode:faultCode
              _description:_description
              callingPersonName:callingPersonName
              callingPersonPhone:callingPersonPhone
              callingPersonEmail:callingPersonEmail
          completionHandler: ^(SWGTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmCreateBreakdownTicketPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***| Client ID | 
 **equipmentId** | **NSString***| Optional UID Of equipment | [optional] 
 **priority** | **NSString***| Optional Priority | [optional] 
 **faultCode** | **NSString***| Fault Code | [optional] 
 **_description** | **NSString***| Optional ticket description or comments | [optional] 
 **callingPersonName** | **NSString***| Optional calling person name | [optional] 
 **callingPersonPhone** | **NSString***| Optional calling person phone number | [optional] 
 **callingPersonEmail** | **NSString***| Optional calling person email_id | [optional] 

### Return type

[**SWGTicket***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmDenyOvertimeRequestPost**
```objc
-(NSURLSessionTask*) bmDenyOvertimeRequestPostWithRequestId: (NSNumber*) requestId
        completionHandler: (void (^)(NSError* error)) handler;
```



Deny a spare request

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* requestId = @789; // The Request ID to deny

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmDenyOvertimeRequestPostWithRequestId:requestId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmDenyOvertimeRequestPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **NSNumber***| The Request ID to deny | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmDenySparesRequestPost**
```objc
-(NSURLSessionTask*) bmDenySparesRequestPostWithRequestId: (NSString*) requestId
        completionHandler: (void (^)(NSError* error)) handler;
```



Deny the Spares request

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* requestId = @"requestId_example"; // request ids to return

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmDenySparesRequestPostWithRequestId:requestId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmDenySparesRequestPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestId** | **NSString***| request ids to return | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmForceAllLocationUpdatePost**
```objc
-(NSURLSessionTask*) bmForceAllLocationUpdatePostWithCompletionHandler: 
        (void (^)(NSError* error)) handler;
```



Force location update for all Technicians of the branch.

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmForceAllLocationUpdatePostWithCompletionHandler: 
          ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmForceAllLocationUpdatePost: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmForceLocationUpdatePost**
```objc
-(NSURLSessionTask*) bmForceLocationUpdatePostWithTechnicianId: (NSNumber*) technicianId
        completionHandler: (void (^)(NSError* error)) handler;
```



Force location update for a particular technician

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* technicianId = @789; // The Technician ID to Force location update

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmForceLocationUpdatePostWithTechnicianId:technicianId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmForceLocationUpdatePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **technicianId** | **NSNumber***| The Technician ID to Force location update | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetAmcTicketAmcTicketIdGet**
```objc
-(NSURLSessionTask*) bmGetAmcTicketAmcTicketIdGetWithAmcTicketId: (NSNumber*) amcTicketId
        completionHandler: (void (^)(SWGGetAmcTicketResult* output, NSError* error)) handler;
```



get the `Ticket` object

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* amcTicketId = @789; // UID of the `AMC Ticket`

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetAmcTicketAmcTicketIdGetWithAmcTicketId:amcTicketId
          completionHandler: ^(SWGGetAmcTicketResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetAmcTicketAmcTicketIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amcTicketId** | **NSNumber***| UID of the &#x60;AMC Ticket&#x60; | 

### Return type

[**SWGGetAmcTicketResult***](SWGGetAmcTicketResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetAmcTicketsGet**
```objc
-(NSURLSessionTask*) bmGetAmcTicketsGetWithLimit: (NSNumber*) limit
    status: (NSString*) status
    clientId: (NSString*) clientId
    technicianId: (NSNumber*) technicianId
        completionHandler: (void (^)(NSArray<SWGAmcTicket>* output, NSError* error)) handler;
```



get a list of `Tickets` **(AMC Type)** that conform to a certain criteria

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* status = @"status_example"; // Otional filter by status (optional)
NSString* clientId = @"clientId_example"; // optional filter by client_id (optional)
NSNumber* technicianId = @789; // optional filter by technician_id (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetAmcTicketsGetWithLimit:limit
              status:status
              clientId:clientId
              technicianId:technicianId
          completionHandler: ^(NSArray<SWGAmcTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetAmcTicketsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **status** | **NSString***| Otional filter by status | [optional] 
 **clientId** | **NSString***| optional filter by client_id | [optional] 
 **technicianId** | **NSNumber***| optional filter by technician_id | [optional] 

### Return type

[**NSArray<SWGAmcTicket>***](SWGAmcTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetAmcTypeClientIdGet**
```objc
-(NSURLSessionTask*) bmGetAmcTypeClientIdGetWithClientId: (NSString*) clientId
        completionHandler: (void (^)(NSString* output, NSError* error)) handler;
```



get the current AMC Type for the `Client`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* clientId = @"clientId_example"; // Equipment ID

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetAmcTypeClientIdGetWithClientId:clientId
          completionHandler: ^(NSString* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetAmcTypeClientIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***| Equipment ID | 

### Return type

**NSString***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetAppVersionGet**
```objc
-(NSURLSessionTask*) bmGetAppVersionGetWithCompletionHandler: 
        (void (^)(SWGAppVersionInfo* output, NSError* error)) handler;
```



Get latest app version

### Example 
```objc


SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetAppVersionGetWithCompletionHandler: 
          ^(SWGAppVersionInfo* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetAppVersionGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGAppVersionInfo***](SWGAppVersionInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetBillingStatusClientIdGet**
```objc
-(NSURLSessionTask*) bmGetBillingStatusClientIdGetWithClientId: (NSString*) clientId
        completionHandler: (void (^)(SWGInlineResponse200* output, NSError* error)) handler;
```



get the current billing status for the `Client`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* clientId = @"clientId_example"; // Equipment ID

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetBillingStatusClientIdGetWithClientId:clientId
          completionHandler: ^(SWGInlineResponse200* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetBillingStatusClientIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***| Equipment ID | 

### Return type

[**SWGInlineResponse200***](SWGInlineResponse200.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetBranchStatusGet**
```objc
-(NSURLSessionTask*) bmGetBranchStatusGetWithCompletionHandler: 
        (void (^)(SWGBranchStatus* output, NSError* error)) handler;
```



get the current AMC Type for the `Plant`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetBranchStatusGetWithCompletionHandler: 
          ^(SWGBranchStatus* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetBranchStatusGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGBranchStatus***](SWGBranchStatus.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetBreakdownTicketTicketIdGet**
```objc
-(NSURLSessionTask*) bmGetBreakdownTicketTicketIdGetWithTicketId: (NSNumber*) ticketId
        completionHandler: (void (^)(SWGGetTicketResult* output, NSError* error)) handler;
```



get the `Ticket` object

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // UID of the `Ticket`

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetBreakdownTicketTicketIdGetWithTicketId:ticketId
          completionHandler: ^(SWGGetTicketResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetBreakdownTicketTicketIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***| UID of the &#x60;Ticket&#x60; | 

### Return type

[**SWGGetTicketResult***](SWGGetTicketResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetBreakdownTicketsGet**
```objc
-(NSURLSessionTask*) bmGetBreakdownTicketsGetWithLimit: (NSNumber*) limit
    status: (NSString*) status
    clientId: (NSString*) clientId
    technicianId: (NSNumber*) technicianId
        completionHandler: (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of `Tickets` that conform to a certain criteria

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* status = @"status_example"; // Otional filter by status (optional)
NSString* clientId = @"clientId_example"; // optional filter by client_id (optional)
NSNumber* technicianId = @789; // optional filter by technician_id (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetBreakdownTicketsGetWithLimit:limit
              status:status
              clientId:clientId
              technicianId:technicianId
          completionHandler: ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetBreakdownTicketsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **status** | **NSString***| Otional filter by status | [optional] 
 **clientId** | **NSString***| optional filter by client_id | [optional] 
 **technicianId** | **NSNumber***| optional filter by technician_id | [optional] 

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetOverdueAmcTicketsGet**
```objc
-(NSURLSessionTask*) bmGetOverdueAmcTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGAmcTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetOverdueAmcTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGAmcTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetOverdueAmcTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGAmcTicket>***](SWGAmcTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetOverdueBreakdownTicketsGet**
```objc
-(NSURLSessionTask*) bmGetOverdueBreakdownTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetOverdueBreakdownTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetOverdueBreakdownTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetOvertimeRequestsGet**
```objc
-(NSURLSessionTask*) bmGetOvertimeRequestsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGOvertimeRequest>* output, NSError* error)) handler;
```



Get overtime requests

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetOvertimeRequestsGetWithCompletionHandler: 
          ^(NSArray<SWGOvertimeRequest>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetOvertimeRequestsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGOvertimeRequest>***](SWGOvertimeRequest.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetPendingSpareRequestsGet**
```objc
-(NSURLSessionTask*) bmGetPendingSpareRequestsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGSpareRequest>* output, NSError* error)) handler;
```



get a list of Spare Requests

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetPendingSpareRequestsGetWithCompletionHandler: 
          ^(NSArray<SWGSpareRequest>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetPendingSpareRequestsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGSpareRequest>***](SWGSpareRequest.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetProfileGet**
```objc
-(NSURLSessionTask*) bmGetProfileGetWithCompletionHandler: 
        (void (^)(SWGBranchManagerShortProfile* output, NSError* error)) handler;
```



Get the profile of a branch manager

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetProfileGetWithCompletionHandler: 
          ^(SWGBranchManagerShortProfile* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetProfileGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGBranchManagerShortProfile***](SWGBranchManagerShortProfile.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetTechnicalFaultCodeGroupItemsGet**
```objc
-(NSURLSessionTask*) bmGetTechnicalFaultCodeGroupItemsGetWithFaultCodeGroup: (NSString*) faultCodeGroup
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* faultCodeGroup = @"faultCodeGroup_example"; // Fault code group ex. C001

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetTechnicalFaultCodeGroupItemsGetWithFaultCodeGroup:faultCodeGroup
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetTechnicalFaultCodeGroupItemsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **faultCodeGroup** | **NSString***| Fault code group ex. C001 | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetTechnicalFaultCodeGroupsGet**
```objc
-(NSURLSessionTask*) bmGetTechnicalFaultCodeGroupsGetWithCompletionHandler: 
        (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetTechnicalFaultCodeGroupsGetWithCompletionHandler: 
          ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetTechnicalFaultCodeGroupsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetTechnicalFaultCodesGet**
```objc
-(NSURLSessionTask*) bmGetTechnicalFaultCodesGetWithCompletionHandler: 
        (void (^)(NSArray<SWGFaultCode>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetTechnicalFaultCodesGetWithCompletionHandler: 
          ^(NSArray<SWGFaultCode>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetTechnicalFaultCodesGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGFaultCode>***](SWGFaultCode.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetTechnicianTechnicianIdGet**
```objc
-(NSURLSessionTask*) bmGetTechnicianTechnicianIdGetWithTechnicianId: (NSNumber*) technicianId
        completionHandler: (void (^)(SWGTechnician* output, NSError* error)) handler;
```



get the `Technician` object

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* technicianId = @789; // UID of the `Technician`

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetTechnicianTechnicianIdGetWithTechnicianId:technicianId
          completionHandler: ^(SWGTechnician* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetTechnicianTechnicianIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **technicianId** | **NSNumber***| UID of the &#x60;Technician&#x60; | 

### Return type

[**SWGTechnician***](SWGTechnician.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetTicketsThisMonthGet**
```objc
-(NSURLSessionTask*) bmGetTicketsThisMonthGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of tickets created in this month

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetTicketsThisMonthGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetTicketsThisMonthGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetUrgentTicketsGet**
```objc
-(NSURLSessionTask*) bmGetUrgentTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of urgent tickets`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetUrgentTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetUrgentTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetUserFacingFaultCodeGroupItemsGet**
```objc
-(NSURLSessionTask*) bmGetUserFacingFaultCodeGroupItemsGetWithFaultCodeGroup: (NSString*) faultCodeGroup
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* faultCodeGroup = @"faultCodeGroup_example"; // Fault code group ex. C001

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetUserFacingFaultCodeGroupItemsGetWithFaultCodeGroup:faultCodeGroup
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetUserFacingFaultCodeGroupItemsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **faultCodeGroup** | **NSString***| Fault code group ex. C001 | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetUserFacingFaultCodeGroupsGet**
```objc
-(NSURLSessionTask*) bmGetUserFacingFaultCodeGroupsGetWithCompletionHandler: 
        (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetUserFacingFaultCodeGroupsGetWithCompletionHandler: 
          ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetUserFacingFaultCodeGroupsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmGetUserFacingFaultCodesGet**
```objc
-(NSURLSessionTask*) bmGetUserFacingFaultCodesGetWithCompletionHandler: 
        (void (^)(NSArray<SWGFaultCode>* output, NSError* error)) handler;
```



get the List of User Facing Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmGetUserFacingFaultCodesGetWithCompletionHandler: 
          ^(NSArray<SWGFaultCode>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmGetUserFacingFaultCodesGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGFaultCode>***](SWGFaultCode.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmListAvailableTechniciansGet**
```objc
-(NSURLSessionTask*) bmListAvailableTechniciansGetWithLimit: (NSNumber*) limit
    regionCode: (NSString*) regionCode
        completionHandler: (void (^)(NSArray<SWGTechnician>* output, NSError* error)) handler;
```



get a list of all `Technicians` who are available today (Marked as present)

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* regionCode = @"regionCode_example"; // optionally filter by region code (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmListAvailableTechniciansGetWithLimit:limit
              regionCode:regionCode
          completionHandler: ^(NSArray<SWGTechnician>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmListAvailableTechniciansGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **regionCode** | **NSString***| optionally filter by region code | [optional] 

### Return type

[**NSArray<SWGTechnician>***](SWGTechnician.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmListTechniciansGet**
```objc
-(NSURLSessionTask*) bmListTechniciansGetWithLimit: (NSNumber*) limit
    regionCode: (NSString*) regionCode
        completionHandler: (void (^)(NSArray<SWGTechnician>* output, NSError* error)) handler;
```



get a list of all `Technicians`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* regionCode = @"regionCode_example"; // optionally filter by region code (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmListTechniciansGetWithLimit:limit
              regionCode:regionCode
          completionHandler: ^(NSArray<SWGTechnician>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmListTechniciansGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **regionCode** | **NSString***| optionally filter by region code | [optional] 

### Return type

[**NSArray<SWGTechnician>***](SWGTechnician.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmMarkTechnicianTodayTicketsPost**
```objc
-(NSURLSessionTask*) bmMarkTechnicianTodayTicketsPostWithTechnicianId: (NSNumber*) technicianId
    todaysBreakdownTicketsIds: (NSArray<NSNumber*>*) todaysBreakdownTicketsIds
    todaysAmcTicketsIds: (NSArray<NSNumber*>*) todaysAmcTicketsIds
        completionHandler: (void (^)(SWGTechnician* output, NSError* error)) handler;
```



Mark Tickets a technician is working on today

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* technicianId = @789; // Technician ID
NSArray<NSNumber*>* todaysBreakdownTicketsIds = @[@56]; // Today's Active Breakdown Tickets
NSArray<NSNumber*>* todaysAmcTicketsIds = @[@56]; // Today's Active AMC Tickets

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmMarkTechnicianTodayTicketsPostWithTechnicianId:technicianId
              todaysBreakdownTicketsIds:todaysBreakdownTicketsIds
              todaysAmcTicketsIds:todaysAmcTicketsIds
          completionHandler: ^(SWGTechnician* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmMarkTechnicianTodayTicketsPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **technicianId** | **NSNumber***| Technician ID | 
 **todaysBreakdownTicketsIds** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Today&#39;s Active Breakdown Tickets | 
 **todaysAmcTicketsIds** | [**NSArray&lt;NSNumber*&gt;***](NSNumber*.md)| Today&#39;s Active AMC Tickets | 

### Return type

[**SWGTechnician***](SWGTechnician.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmNotifyChatMessagePost**
```objc
-(NSURLSessionTask*) bmNotifyChatMessagePostWithUserId: (NSNumber*) userId
    chatUid: (NSString*) chatUid
    message: (NSString*) message
        completionHandler: (void (^)(NSError* error)) handler;
```



Send Chat Notification

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* userId = @789; // The User ID to Send notification to
NSString* chatUid = @"chatUid_example"; // The Chat UID
NSString* message = @"message_example"; // The chat message

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmNotifyChatMessagePostWithUserId:userId
              chatUid:chatUid
              message:message
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmNotifyChatMessagePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **NSNumber***| The User ID to Send notification to | 
 **chatUid** | **NSString***| The Chat UID | 
 **message** | **NSString***| The chat message | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmQueryClientsNonDistinctGet**
```objc
-(NSURLSessionTask*) bmQueryClientsNonDistinctGetWithClientName: (NSString*) clientName
    city: (NSString*) city
        completionHandler: (void (^)(NSArray<SWGClientQueryResult>* output, NSError* error)) handler;
```



get a list of all `Clients` and their associated `equipment_ids` that match the query

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* clientName = @"clientName_example"; // client full name search
NSString* city = @"city_example"; // the city in which to search for

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmQueryClientsNonDistinctGetWithClientName:clientName
              city:city
          completionHandler: ^(NSArray<SWGClientQueryResult>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmQueryClientsNonDistinctGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientName** | **NSString***| client full name search | 
 **city** | **NSString***| the city in which to search for | 

### Return type

[**NSArray<SWGClientQueryResult>***](SWGClientQueryResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmQueryDistinctClientCityGet**
```objc
-(NSURLSessionTask*) bmQueryDistinctClientCityGetWithFullClientName: (NSString*) fullClientName
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get a list of all `Clients` and their associated `equipment_ids` that match the query

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* fullClientName = @"fullClientName_example"; // client full name

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmQueryDistinctClientCityGetWithFullClientName:fullClientName
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmQueryDistinctClientCityGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fullClientName** | **NSString***| client full name | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmQueryDistinctClientNameGet**
```objc
-(NSURLSessionTask*) bmQueryDistinctClientNameGetWithQuery: (NSString*) query
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get a list of all distinct Client names with a partial name search

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* query = @"query_example"; // client partial search

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmQueryDistinctClientNameGetWithQuery:query
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmQueryDistinctClientNameGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **NSString***| client partial search | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUnAssignTechnicianPost**
```objc
-(NSURLSessionTask*) bmUnAssignTechnicianPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    technicianId: (NSNumber*) technicianId
        completionHandler: (void (^)(NSError* error)) handler;
```



Assign a `Technician` to a `Ticket`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // The `ticketId` of the ticket to assign
NSNumber* technicianId = @789; // UID of the technician to assign

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUnAssignTechnicianPostWithTicketType:ticketType
              ticketId:ticketId
              technicianId:technicianId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUnAssignTechnicianPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| The &#x60;ticketId&#x60; of the ticket to assign | 
 **technicianId** | **NSNumber***| UID of the technician to assign | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUpdateFaultCodePost**
```objc
-(NSURLSessionTask*) bmUpdateFaultCodePostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    technicalFaultCode: (NSString*) technicalFaultCode
        completionHandler: (void (^)(NSError* error)) handler;
```



Update the fault code

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id for which this report is uploaded
NSString* technicalFaultCode = @"technicalFaultCode_example"; // Predefined fault code

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUpdateFaultCodePostWithTicketType:ticketType
              ticketId:ticketId
              technicalFaultCode:technicalFaultCode
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUpdateFaultCodePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id for which this report is uploaded | 
 **technicalFaultCode** | **NSString***| Predefined fault code | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUpdateGcmRegIdPost**
```objc
-(NSURLSessionTask*) bmUpdateGcmRegIdPostWithRegId: (NSString*) regId
        completionHandler: (void (^)(NSError* error)) handler;
```



Update the **GCM google cloud messaging** *reg_id* for branch manager

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* regId = @"regId_example"; // Fresh GCM Reg_id

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUpdateGcmRegIdPostWithRegId:regId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUpdateGcmRegIdPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regId** | **NSString***| Fresh GCM Reg_id | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUpdateProfilePicturePost**
```objc
-(NSURLSessionTask*) bmUpdateProfilePicturePostWithPicture: (NSURL*) picture
        completionHandler: (void (^)(NSError* error)) handler;
```



update the profile pic

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSURL* picture = [NSURL fileURLWithPath:@"/path/to/file.txt"]; // New Profile Picture. jpeg/jpg/png

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUpdateProfilePicturePostWithPicture:picture
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUpdateProfilePicturePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picture** | **NSURL***| New Profile Picture. jpeg/jpg/png | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUpdateTicketStatusPost**
```objc
-(NSURLSessionTask*) bmUpdateTicketStatusPostWithTicketId: (NSNumber*) ticketId
    ticketType: (NSString*) ticketType
    status: (NSString*) status
        completionHandler: (void (^)(NSError* error)) handler;
```



Update `Ticket` Status

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // 
NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSString* status = @"status_example"; // Otional filter by status (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUpdateTicketStatusPostWithTicketId:ticketId
              ticketType:ticketType
              status:status
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUpdateTicketStatusPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***|  | 
 **ticketType** | **NSString***| the type of the ticket in question | 
 **status** | **NSString***| Otional filter by status | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **bmUploadReportPost**
```objc
-(NSURLSessionTask*) bmUploadReportPostWithTicketType: (NSString*) ticketType
    file: (NSURL*) file
    ticketId: (NSNumber*) ticketId
    _description: (NSString*) _description
        completionHandler: (void (^)(NSError* error)) handler;
```



upload a report file

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSURL* file = [NSURL fileURLWithPath:@"/path/to/file.txt"]; // The jpeg file to be uploaded
NSNumber* ticketId = @789; // the `Ticket` id for which this report is uploaded
NSString* _description = @"_description_example"; // the details of the uploaded file (optional)

SWGBranchManagerApi*apiInstance = [[SWGBranchManagerApi alloc] init];

[apiInstance bmUploadReportPostWithTicketType:ticketType
              file:file
              ticketId:ticketId
              _description:_description
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGBranchManagerApi->bmUploadReportPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **file** | **NSURL***| The jpeg file to be uploaded | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id for which this report is uploaded | 
 **_description** | **NSString***| the details of the uploaded file | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

