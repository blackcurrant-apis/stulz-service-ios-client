# SWGBranchManagerShortProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branchManagerId** | **NSNumber*** | UID for the technician | 
**branchManagerUserId** | **NSNumber*** | Foreign id to the USER Table | 
**branchManagerEmailId** | **NSString*** | email id of the branch manager | [optional] 
**branchManagerName** | **NSString*** | name of the Technician | 
**branchManagerPlantCode** | **NSString*** | plant code to which the Technician is assigned | 
**branchManagerPhone** | **NSString*** | phone number of the Technician | 
**branchManagerProfilePicUrl** | **NSString*** | url to profile picture | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


