# SWGBranchStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketsCreated** | **NSNumber*** |  | [optional] 
**amcTicketsCreated** | **NSNumber*** |  | [optional] 
**ticketsInProgress** | **NSNumber*** |  | [optional] 
**amcTicketsInProgress** | **NSNumber*** |  | [optional] 
**ticketsOnHold** | **NSNumber*** |  | [optional] 
**amcTicketsOnHold** | **NSNumber*** |  | [optional] 
**ticketsClosed** | **NSNumber*** |  | [optional] 
**amcTicketsClosed** | **NSNumber*** |  | [optional] 
**ticketsResolved** | **NSNumber*** |  | [optional] 
**amcTicketsResolved** | **NSNumber*** |  | [optional] 
**ticketsUrgent** | **NSNumber*** |  | [optional] 
**ticketsOverdue** | **NSNumber*** |  | [optional] 
**ticketsServicedThisMonth** | **NSNumber*** |  | [optional] 
**amcTicketsServicedThisMonth** | **NSNumber*** |  | [optional] 
**pendingSpareApprovals** | **NSNumber*** |  | [optional] 
**pendingOvertimeApprovals** | **NSNumber*** |  | [optional] 
**techniciansTotal** | **NSNumber*** |  | [optional] 
**techniciansAvailableToday** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


