# SWGClient

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **NSString*** | UID of Client | 
**clientName** | **NSString*** | name of Client | 
**postalCode** | **NSString*** |  | [optional] 
**clientPlantCode** | **NSString*** | UID of the plant that services this client | 
**clientEmail** | **NSString*** | email of Client | [optional] 
**clientPhone** | **NSString*** | phone of Client | [optional] 
**clientCity** | **NSString*** | city of Client | [optional] 
**clientLatitude** | **NSNumber*** | The location latitude where the client is located | 
**clientLongitude** | **NSNumber*** | The location longitude where the client is located | 
**wbsDesc** | **NSString*** | Dont know but important field | [optional] 
**createdAt** | **NSString*** | Timestamp when the Client was created | [optional] 
**updatedAt** | **NSString*** | Timestamp when the Client was last | [optional] 
**createdBy** | **NSString*** |  | [optional] 
**updatedBy** | **NSString*** |  | [optional] 
**canAccess** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


