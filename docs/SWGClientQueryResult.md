# SWGClientQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**SWGClient***](SWGClient.md) |  | [optional] 
**equipments** | [**NSArray&lt;SWGClientQueryResultEquipments&gt;***](SWGClientQueryResultEquipments.md) | list of equipments | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


