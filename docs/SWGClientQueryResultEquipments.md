# SWGClientQueryResultEquipments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**equipId** | **NSString*** | Equipment UID | [optional] 
**modelNumber** | **NSString*** | string | [optional] 
**serialNumber** | **NSString*** | Sereal No | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


