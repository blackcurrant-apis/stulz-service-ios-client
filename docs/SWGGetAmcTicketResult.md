# SWGGetAmcTicketResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticket** | [**SWGAmcTicket***](SWGAmcTicket.md) |  | [optional] 
**assignedTechnicians** | [**NSArray&lt;SWGTechnicianShortProfile&gt;***](SWGTechnicianShortProfile.md) | list of assigned technicians | [optional] 
**reports** | [**NSArray&lt;SWGReport&gt;***](SWGReport.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


