# SWGInlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingStatus** | **NSString*** | current billing status | [optional] 
**elapsedDays** | **NSNumber*** | days since payment is due. 0 if paid. | [optional] 
**lastPaidDate** | **NSString*** | date when last payment was made | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


