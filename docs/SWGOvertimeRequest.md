# SWGOvertimeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**technicianId** | **NSNumber*** | UID for the technician | [optional] 
**technicianProfile** | [**SWGTechnicianShortProfile***](SWGTechnicianShortProfile.md) |  | [optional] 
**overtimeStartTime** | **NSString*** |  | [optional] 
**overtimeEndTime** | **NSString*** |  | [optional] 
**overtimeRequestedHours** | **NSNumber*** |  | [optional] 
**overtimeApprovedHours** | **NSNumber*** |  | [optional] 
**overtimeRequestId** | **NSNumber*** | unique request id | [optional] 
**overtimeComment** | **NSString*** | A description filled by the technician about the overtime request. | [optional] 
**overtimeRequestLat** | **NSNumber*** | optional latitude of the overtime request | [optional] 
**overtimeRequestLon** | **NSNumber*** | optional longitude of the overtime request | [optional] 
**overtimeRequestTimestamp** | **NSString*** | Timestamp of request | [optional] 
**ticketType** | **NSString*** |  | [optional] 
**ticketId** | **NSNumber*** | UID of ticket | [optional] 
**requestStatus** | **NSString*** | current request status | [optional] 
**wasInGeofence** | **NSNumber*** |  | [optional] [default to @(NO)]
**distanceFromClient** | **NSNumber*** | In Km. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


