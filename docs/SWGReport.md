# SWGReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reportId** | **NSString*** | Unique identifier for the report | [optional] 
**_description** | **NSString*** |  | [optional] 
**fileUrl** | **NSString*** | the uploaded file | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


