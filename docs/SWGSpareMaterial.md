# SWGSpareMaterial

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**materialId** | **NSString*** | unique identifier for the material | [optional] 
**materialDescription** | **NSString*** | name of the material | [optional] 
**materialBillingUnit** | **NSString*** | how is the material measured | [optional] 
**materialType** | **NSString*** | FINISHED / RAW MATERIAL | [optional] 
**materialGroup** | **NSString*** | the material group to which the material belongs | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


