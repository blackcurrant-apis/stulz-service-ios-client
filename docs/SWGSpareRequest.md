# SWGSpareRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestTicketId** | **NSNumber*** | UID of ticket | [optional] 
**requestClientName** | **NSString*** |  | [optional] 
**requestEquipmentId** | **NSString*** |  | [optional] 
**requestId** | **NSString*** | unique id per requested item | [optional] 
**oldSerialNo** | **NSString*** | Old serial number. Beware, might be null. | [optional] 
**varNewSerialNo** | **NSString*** | New serial number. Beware, might be null. | [optional] 
**material** | [**SWGSpareMaterial***](SWGSpareMaterial.md) |  | [optional] 
**requestStatus** | **NSString*** | current request status | [optional] 
**requestComment** | **NSString*** | the comment technician entered while generating the request. Might be null | [optional] 
**approvedQuantity** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


