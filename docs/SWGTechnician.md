# SWGTechnician

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**technicianId** | **NSNumber*** | UID for the technician | 
**technicianUserId** | **NSNumber*** | Foreign id to the USER Table | 
**technicianName** | **NSString*** | name of the Technician | 
**technicianRegionCode** | **NSArray&lt;NSString*&gt;*** | region codes to which the Technician is assigned | [optional] 
**technicianPhone** | **NSString*** | phone number of the Technician | 
**createdAt** | **NSString*** | Timestamp when the Technician was first created | 
**createdBy** | **NSNumber*** | The User UID who created the Technician | 
**technicianAddress** | **NSString*** | home address of the Technician | [optional] 
**technicianPostCode** | **NSNumber*** | postal code for the | [optional] 
**updatedAt** | **NSString*** | Timestamp when the Technician was last updated | [optional] 
**updatedBy** | **NSNumber*** | The User UID who updated the Technician Last | [optional] 
**technicianProfilePicUrl** | **NSString*** | url to profile picture | [optional] 
**technicianWorkingDays** | **NSString*** | Days does the technician works on. | 
**technicianStartTime** | **NSString*** | The default time when a technician starts his work. | 
**technicianPlantCodes** | **NSArray&lt;NSString*&gt;*** | the list of plant codes to which the live location of the technician will be stored. | [optional] 
**technicianLastLogin** | **NSString*** | to be depricated | [optional] 
**technicianIsAvailableToday** | **NSNumber*** |  | [optional] 
**technicianActiveBreakdownTickets** | [**NSArray&lt;SWGActiveTicket&gt;***](SWGActiveTicket.md) |  | [optional] 
**technicianActiveAmcTickets** | [**NSArray&lt;SWGActiveTicket&gt;***](SWGActiveTicket.md) |  | [optional] 
**technicianTicketsWorkingOnToday** | [**NSArray&lt;SWGActiveTicket&gt;***](SWGActiveTicket.md) |  | [optional] 
**loginTimestamp** | **NSString*** |  | [optional] 
**loginLat** | **NSNumber*** |  | [optional] 
**loginLon** | **NSNumber*** |  | [optional] 
**logoutTimestamp** | **NSString*** |  | [optional] 
**logoutLat** | **NSNumber*** |  | [optional] 
**logoutLon** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


