# SWGTechnicianApi

All URIs are relative to *http://stulzdemo.com/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**technicianAddTicketCommentPost**](SWGTechnicianApi.md#technicianaddticketcommentpost) | **POST** /technician/add_ticket_comment | 
[**technicianCloseTicketPost**](SWGTechnicianApi.md#techniciancloseticketpost) | **POST** /technician/close_ticket | 
[**technicianGetAmcTicketAmcTicketIdGet**](SWGTechnicianApi.md#techniciangetamcticketamcticketidget) | **GET** /technician/get_amc_ticket/{amc_ticket_id} | 
[**technicianGetAmcTicketsGet**](SWGTechnicianApi.md#techniciangetamcticketsget) | **GET** /technician/get_amc_tickets | 
[**technicianGetAppVersionGet**](SWGTechnicianApi.md#techniciangetappversionget) | **GET** /technician/get_app_version | 
[**technicianGetBreakdownTicketTicketIdGet**](SWGTechnicianApi.md#techniciangetbreakdownticketticketidget) | **GET** /technician/get_breakdown_ticket/{ticket_id} | 
[**technicianGetBreakdownTicketsGet**](SWGTechnicianApi.md#techniciangetbreakdownticketsget) | **GET** /technician/get_breakdown_tickets | 
[**technicianGetEquipmentsGet**](SWGTechnicianApi.md#techniciangetequipmentsget) | **GET** /technician/get_equipments | 
[**technicianGetOverdueAmcTicketsGet**](SWGTechnicianApi.md#techniciangetoverdueamcticketsget) | **GET** /technician/get_overdue_amc_tickets | 
[**technicianGetOverdueBreakdownTicketsGet**](SWGTechnicianApi.md#techniciangetoverduebreakdownticketsget) | **GET** /technician/get_overdue_breakdown_tickets | 
[**technicianGetPendingSpareRequestsGet**](SWGTechnicianApi.md#techniciangetpendingsparerequestsget) | **GET** /technician/get_pending_spare_requests | 
[**technicianGetProfileGet**](SWGTechnicianApi.md#techniciangetprofileget) | **GET** /technician/getProfile | 
[**technicianGetSparesGet**](SWGTechnicianApi.md#techniciangetsparesget) | **GET** /technician/get_spares | 
[**technicianGetSparesGroupsGet**](SWGTechnicianApi.md#techniciangetsparesgroupsget) | **GET** /technician/get_spares_groups | 
[**technicianGetStatusGet**](SWGTechnicianApi.md#techniciangetstatusget) | **GET** /technician/get_status | 
[**technicianGetTechnicalFaultCodeGroupItemsGet**](SWGTechnicianApi.md#techniciangettechnicalfaultcodegroupitemsget) | **GET** /technician/get_technical_fault_code_group_items | 
[**technicianGetTechnicalFaultCodeGroupsGet**](SWGTechnicianApi.md#techniciangettechnicalfaultcodegroupsget) | **GET** /technician/get_technical_fault_code_groups | 
[**technicianGetTechnicalFaultCodesGet**](SWGTechnicianApi.md#techniciangettechnicalfaultcodesget) | **GET** /technician/get_technical_fault_codes | 
[**technicianGetTodaysAmcTicketsGet**](SWGTechnicianApi.md#techniciangettodaysamcticketsget) | **GET** /technician/get_todays_amc_tickets | 
[**technicianGetTodaysBreakdownTicketsGet**](SWGTechnicianApi.md#techniciangettodaysbreakdownticketsget) | **GET** /technician/get_todays_breakdown_tickets | 
[**technicianGetUrgentTicketsGet**](SWGTechnicianApi.md#techniciangeturgentticketsget) | **GET** /technician/get_urgent_tickets | 
[**technicianLocationCheckInPost**](SWGTechnicianApi.md#technicianlocationcheckinpost) | **POST** /technician/location_check_in | 
[**technicianLocationCheckOutPost**](SWGTechnicianApi.md#technicianlocationcheckoutpost) | **POST** /technician/location_check_out | 
[**technicianNotifyChatMessagePost**](SWGTechnicianApi.md#techniciannotifychatmessagepost) | **POST** /technician/notify_chat_message | 
[**technicianRecordLoginPost**](SWGTechnicianApi.md#technicianrecordloginpost) | **POST** /technician/record_login | 
[**technicianRecordLogoutPost**](SWGTechnicianApi.md#technicianrecordlogoutpost) | **POST** /technician/record_logout | 
[**technicianRequestOvertimePost**](SWGTechnicianApi.md#technicianrequestovertimepost) | **POST** /technician/request_overtime | 
[**technicianRequestSparePost**](SWGTechnicianApi.md#technicianrequestsparepost) | **POST** /technician/request_spare | 
[**technicianUpdateEquipmentIdPost**](SWGTechnicianApi.md#technicianupdateequipmentidpost) | **POST** /technician/update_equipment_id | 
[**technicianUpdateFaultCodePost**](SWGTechnicianApi.md#technicianupdatefaultcodepost) | **POST** /technician/update_fault_code | 
[**technicianUpdateGcmRegIdPost**](SWGTechnicianApi.md#technicianupdategcmregidpost) | **POST** /technician/update_gcm_reg_id | 
[**technicianUpdateProfilePicturePost**](SWGTechnicianApi.md#technicianupdateprofilepicturepost) | **POST** /technician/update_profile_picture | 
[**technicianUpdateTicketStatusPost**](SWGTechnicianApi.md#technicianupdateticketstatuspost) | **POST** /technician/update_ticket_status | 
[**technicianUploadReportPost**](SWGTechnicianApi.md#technicianuploadreportpost) | **POST** /technician/upload_report | 


# **technicianAddTicketCommentPost**
```objc
-(NSURLSessionTask*) technicianAddTicketCommentPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    comment: (NSString*) comment
        completionHandler: (void (^)(NSError* error)) handler;
```



add comment to ticket

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // UID of the `Ticket`
NSString* comment = @"comment_example"; // comment to add

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianAddTicketCommentPostWithTicketType:ticketType
              ticketId:ticketId
              comment:comment
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianAddTicketCommentPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| UID of the &#x60;Ticket&#x60; | 
 **comment** | **NSString***| comment to add | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianCloseTicketPost**
```objc
-(NSURLSessionTask*) technicianCloseTicketPostWithTicketId: (NSNumber*) ticketId
    ticketType: (NSString*) ticketType
        completionHandler: (void (^)(NSError* error)) handler;
```



Close ticket from pov of technician

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // 
NSString* ticketType = @"ticketType_example"; // the type of the ticket in question

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianCloseTicketPostWithTicketId:ticketId
              ticketType:ticketType
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianCloseTicketPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***|  | 
 **ticketType** | **NSString***| the type of the ticket in question | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetAmcTicketAmcTicketIdGet**
```objc
-(NSURLSessionTask*) technicianGetAmcTicketAmcTicketIdGetWithAmcTicketId: (NSNumber*) amcTicketId
        completionHandler: (void (^)(SWGGetAmcTicketResult* output, NSError* error)) handler;
```



get the `Ticket` object

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* amcTicketId = @789; // UID of the `Ticket`

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetAmcTicketAmcTicketIdGetWithAmcTicketId:amcTicketId
          completionHandler: ^(SWGGetAmcTicketResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetAmcTicketAmcTicketIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amcTicketId** | **NSNumber***| UID of the &#x60;Ticket&#x60; | 

### Return type

[**SWGGetAmcTicketResult***](SWGGetAmcTicketResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetAmcTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetAmcTicketsGetWithLimit: (NSNumber*) limit
    status: (NSString*) status
    clientId: (NSString*) clientId
        completionHandler: (void (^)(NSArray<SWGAmcTicket>* output, NSError* error)) handler;
```



get a list of `Tickets` that conform to a certain criteria

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* status = @"status_example"; // Otional filter by status (optional)
NSString* clientId = @"clientId_example"; // optional filter by client_id (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetAmcTicketsGetWithLimit:limit
              status:status
              clientId:clientId
          completionHandler: ^(NSArray<SWGAmcTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetAmcTicketsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **status** | **NSString***| Otional filter by status | [optional] 
 **clientId** | **NSString***| optional filter by client_id | [optional] 

### Return type

[**NSArray<SWGAmcTicket>***](SWGAmcTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetAppVersionGet**
```objc
-(NSURLSessionTask*) technicianGetAppVersionGetWithCompletionHandler: 
        (void (^)(SWGAppVersionInfo* output, NSError* error)) handler;
```



Get latest app version

### Example 
```objc


SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetAppVersionGetWithCompletionHandler: 
          ^(SWGAppVersionInfo* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetAppVersionGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGAppVersionInfo***](SWGAppVersionInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetBreakdownTicketTicketIdGet**
```objc
-(NSURLSessionTask*) technicianGetBreakdownTicketTicketIdGetWithTicketId: (NSNumber*) ticketId
        completionHandler: (void (^)(SWGGetTicketResult* output, NSError* error)) handler;
```



get the `Ticket` object

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // UID of the `Ticket`

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetBreakdownTicketTicketIdGetWithTicketId:ticketId
          completionHandler: ^(SWGGetTicketResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetBreakdownTicketTicketIdGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***| UID of the &#x60;Ticket&#x60; | 

### Return type

[**SWGGetTicketResult***](SWGGetTicketResult.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetBreakdownTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetBreakdownTicketsGetWithLimit: (NSNumber*) limit
    status: (NSString*) status
    clientId: (NSString*) clientId
        completionHandler: (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of `Tickets` that conform to a certain criteria

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* limit = @56; // optional limit on number of entries (optional)
NSString* status = @"status_example"; // Otional filter by status (optional)
NSString* clientId = @"clientId_example"; // optional filter by client_id (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetBreakdownTicketsGetWithLimit:limit
              status:status
              clientId:clientId
          completionHandler: ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetBreakdownTicketsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **NSNumber***| optional limit on number of entries | [optional] 
 **status** | **NSString***| Otional filter by status | [optional] 
 **clientId** | **NSString***| optional filter by client_id | [optional] 

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetEquipmentsGet**
```objc
-(NSURLSessionTask*) technicianGetEquipmentsGetWithClientId: (NSString*) clientId
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the Equipment list of a client

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* clientId = @"clientId_example"; // The Client ID to list equipments of

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetEquipmentsGetWithClientId:clientId
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetEquipmentsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***| The Client ID to list equipments of | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetOverdueAmcTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetOverdueAmcTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGAmcTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetOverdueAmcTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGAmcTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetOverdueAmcTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGAmcTicket>***](SWGAmcTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetOverdueBreakdownTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetOverdueBreakdownTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetOverdueBreakdownTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetOverdueBreakdownTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetPendingSpareRequestsGet**
```objc
-(NSURLSessionTask*) technicianGetPendingSpareRequestsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGSpareRequest>* output, NSError* error)) handler;
```



get a list of Spare Requests

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetPendingSpareRequestsGetWithCompletionHandler: 
          ^(NSArray<SWGSpareRequest>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetPendingSpareRequestsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGSpareRequest>***](SWGSpareRequest.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetProfileGet**
```objc
-(NSURLSessionTask*) technicianGetProfileGetWithCompletionHandler: 
        (void (^)(SWGTechnician* output, NSError* error)) handler;
```



Get the short profile of a technician. **working days** Days does the technician works on. Its a 7 letter string with each character representing each day. Starting at Monday, ending at Sunday. Character is P if present, A is absent. eg. PPPPPAA would mean working for all days except weekends. **start time** The default time when a technician starts his work. A notification to check in will be given on the app to mark attendance and check in to work at this time. Format will be HH:MM where Hours is 24H format. eg. 15:30, 08:15 

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetProfileGetWithCompletionHandler: 
          ^(SWGTechnician* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetProfileGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGTechnician***](SWGTechnician.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetSparesGet**
```objc
-(NSURLSessionTask*) technicianGetSparesGetWithGroup: (NSString*) group
        completionHandler: (void (^)(NSArray<SWGSpareMaterial>* output, NSError* error)) handler;
```



get the Spares for a material group

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* group = @"group_example"; // 

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetSparesGetWithGroup:group
          completionHandler: ^(NSArray<SWGSpareMaterial>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetSparesGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **NSString***|  | 

### Return type

[**NSArray<SWGSpareMaterial>***](SWGSpareMaterial.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetSparesGroupsGet**
```objc
-(NSURLSessionTask*) technicianGetSparesGroupsGetWithCompletionHandler: 
        (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



Get all possible spare groups

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetSparesGroupsGetWithCompletionHandler: 
          ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetSparesGroupsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetStatusGet**
```objc
-(NSURLSessionTask*) technicianGetStatusGetWithCompletionHandler: 
        (void (^)(SWGTechnicianStatus* output, NSError* error)) handler;
```



Dashboard Contents for Technician

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetStatusGetWithCompletionHandler: 
          ^(SWGTechnicianStatus* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetStatusGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SWGTechnicianStatus***](SWGTechnicianStatus.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetTechnicalFaultCodeGroupItemsGet**
```objc
-(NSURLSessionTask*) technicianGetTechnicalFaultCodeGroupItemsGetWithFaultCodeGroup: (NSString*) faultCodeGroup
        completionHandler: (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* faultCodeGroup = @"faultCodeGroup_example"; // Fault code group ex. C001

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetTechnicalFaultCodeGroupItemsGetWithFaultCodeGroup:faultCodeGroup
          completionHandler: ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetTechnicalFaultCodeGroupItemsGet: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **faultCodeGroup** | **NSString***| Fault code group ex. C001 | 

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetTechnicalFaultCodeGroupsGet**
```objc
-(NSURLSessionTask*) technicianGetTechnicalFaultCodeGroupsGetWithCompletionHandler: 
        (void (^)(NSArray<NSString*>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetTechnicalFaultCodeGroupsGetWithCompletionHandler: 
          ^(NSArray<NSString*>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetTechnicalFaultCodeGroupsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSArray<NSString*>***

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetTechnicalFaultCodesGet**
```objc
-(NSURLSessionTask*) technicianGetTechnicalFaultCodesGetWithCompletionHandler: 
        (void (^)(NSArray<SWGFaultCode>* output, NSError* error)) handler;
```



get the List of Technical Fault Codes

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetTechnicalFaultCodesGetWithCompletionHandler: 
          ^(NSArray<SWGFaultCode>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetTechnicalFaultCodesGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGFaultCode>***](SWGFaultCode.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetTodaysAmcTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetTodaysAmcTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGAmcTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetTodaysAmcTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGAmcTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetTodaysAmcTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGAmcTicket>***](SWGAmcTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetTodaysBreakdownTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetTodaysBreakdownTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of overdue tickets

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetTodaysBreakdownTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetTodaysBreakdownTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianGetUrgentTicketsGet**
```objc
-(NSURLSessionTask*) technicianGetUrgentTicketsGetWithCompletionHandler: 
        (void (^)(NSArray<SWGTicket>* output, NSError* error)) handler;
```



get a list of urgent tickets`

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];



SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianGetUrgentTicketsGetWithCompletionHandler: 
          ^(NSArray<SWGTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianGetUrgentTicketsGet: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NSArray<SWGTicket>***](SWGTicket.md)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianLocationCheckInPost**
```objc
-(NSURLSessionTask*) technicianLocationCheckInPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
        completionHandler: (void (^)(NSError* error)) handler;
```



Geofence triggered check-in for a ticket

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id of the Ticket in Vicinity

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianLocationCheckInPostWithTicketType:ticketType
              ticketId:ticketId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianLocationCheckInPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id of the Ticket in Vicinity | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianLocationCheckOutPost**
```objc
-(NSURLSessionTask*) technicianLocationCheckOutPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
        completionHandler: (void (^)(NSError* error)) handler;
```



Geofence triggered check-out for a ticket

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id of the Ticket in Vicinity

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianLocationCheckOutPostWithTicketType:ticketType
              ticketId:ticketId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianLocationCheckOutPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id of the Ticket in Vicinity | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianNotifyChatMessagePost**
```objc
-(NSURLSessionTask*) technicianNotifyChatMessagePostWithUserId: (NSNumber*) userId
    chatUid: (NSString*) chatUid
    message: (NSString*) message
        completionHandler: (void (^)(NSError* error)) handler;
```



Send Chat Notification

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* userId = @789; // The User ID to Send notification to
NSString* chatUid = @"chatUid_example"; // The Chat UID
NSString* message = @"message_example"; // The chat message

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianNotifyChatMessagePostWithUserId:userId
              chatUid:chatUid
              message:message
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianNotifyChatMessagePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **NSNumber***| The User ID to Send notification to | 
 **chatUid** | **NSString***| The Chat UID | 
 **message** | **NSString***| The chat message | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianRecordLoginPost**
```objc
-(NSURLSessionTask*) technicianRecordLoginPostWithLat: (NSNumber*) lat
    lon: (NSNumber*) lon
        completionHandler: (void (^)(NSError* error)) handler;
```



Log location when a technician checks in

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* lat = @1.2; // 
NSNumber* lon = @1.2; // 

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianRecordLoginPostWithLat:lat
              lon:lon
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianRecordLoginPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **NSNumber***|  | 
 **lon** | **NSNumber***|  | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianRecordLogoutPost**
```objc
-(NSURLSessionTask*) technicianRecordLogoutPostWithLat: (NSNumber*) lat
    lon: (NSNumber*) lon
        completionHandler: (void (^)(NSError* error)) handler;
```



Log location when a technician checks out

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* lat = @1.2; // 
NSNumber* lon = @1.2; // 

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianRecordLogoutPostWithLat:lat
              lon:lon
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianRecordLogoutPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lat** | **NSNumber***|  | 
 **lon** | **NSNumber***|  | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianRequestOvertimePost**
```objc
-(NSURLSessionTask*) technicianRequestOvertimePostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    comment: (NSString*) comment
    startTime: (NSString*) startTime
    endTime: (NSString*) endTime
    lat: (NSNumber*) lat
    lon: (NSNumber*) lon
        completionHandler: (void (^)(NSError* error)) handler;
```



Request for overtime

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id of the Ticket being serviced
NSString* comment = @"comment_example"; // Comment on why this overtime is needed
NSString* startTime = @"startTime_example"; // Start Time of overtime 24Hr Format HH:MM
NSString* endTime = @"endTime_example"; // End Time of overtime 24Hr Format HH:MM
NSNumber* lat = @1.2; // Optional Latitude of the request (optional)
NSNumber* lon = @1.2; // Optional Longitude of the request (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianRequestOvertimePostWithTicketType:ticketType
              ticketId:ticketId
              comment:comment
              startTime:startTime
              endTime:endTime
              lat:lat
              lon:lon
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianRequestOvertimePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id of the Ticket being serviced | 
 **comment** | **NSString***| Comment on why this overtime is needed | 
 **startTime** | **NSString***| Start Time of overtime 24Hr Format HH:MM | 
 **endTime** | **NSString***| End Time of overtime 24Hr Format HH:MM | 
 **lat** | **NSNumber***| Optional Latitude of the request | [optional] 
 **lon** | **NSNumber***| Optional Longitude of the request | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianRequestSparePost**
```objc
-(NSURLSessionTask*) technicianRequestSparePostWithTicketId: (NSNumber*) ticketId
    equipmentId: (NSString*) equipmentId
    materialId: (NSString*) materialId
    oldSerialNo: (NSString*) oldSerialNo
    comment: (NSString*) comment
        completionHandler: (void (^)(NSError* error)) handler;
```



Generate a request for spares

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // the `Ticket` id of the Ticket being serviced
NSString* equipmentId = @"equipmentId_example"; // the `equipment_id` id of the Ticket being serviced
NSString* materialId = @"materialId_example"; // the spare parts required
NSString* oldSerialNo = @"oldSerialNo_example"; // The old part's serial number number which needs to be replaced (optional)
NSString* comment = @"comment_example"; // comment on why this spare is required (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianRequestSparePostWithTicketId:ticketId
              equipmentId:equipmentId
              materialId:materialId
              oldSerialNo:oldSerialNo
              comment:comment
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianRequestSparePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id of the Ticket being serviced | 
 **equipmentId** | **NSString***| the &#x60;equipment_id&#x60; id of the Ticket being serviced | 
 **materialId** | **NSString***| the spare parts required | 
 **oldSerialNo** | **NSString***| The old part&#39;s serial number number which needs to be replaced | [optional] 
 **comment** | **NSString***| comment on why this spare is required | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUpdateEquipmentIdPost**
```objc
-(NSURLSessionTask*) technicianUpdateEquipmentIdPostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    equipmentId: (NSString*) equipmentId
        completionHandler: (void (^)(NSError* error)) handler;
```



get the Equipment list of a client

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id for which this report is uploaded
NSString* equipmentId = @"equipmentId_example"; // The updated equipment ID

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUpdateEquipmentIdPostWithTicketType:ticketType
              ticketId:ticketId
              equipmentId:equipmentId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUpdateEquipmentIdPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id for which this report is uploaded | 
 **equipmentId** | **NSString***| The updated equipment ID | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUpdateFaultCodePost**
```objc
-(NSURLSessionTask*) technicianUpdateFaultCodePostWithTicketType: (NSString*) ticketType
    ticketId: (NSNumber*) ticketId
    technicalFaultCode: (NSString*) technicalFaultCode
        completionHandler: (void (^)(NSError* error)) handler;
```



Update the fault code

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSNumber* ticketId = @789; // the `Ticket` id for which this report is uploaded
NSString* technicalFaultCode = @"technicalFaultCode_example"; // Predefined fault code

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUpdateFaultCodePostWithTicketType:ticketType
              ticketId:ticketId
              technicalFaultCode:technicalFaultCode
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUpdateFaultCodePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id for which this report is uploaded | 
 **technicalFaultCode** | **NSString***| Predefined fault code | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUpdateGcmRegIdPost**
```objc
-(NSURLSessionTask*) technicianUpdateGcmRegIdPostWithRegId: (NSString*) regId
        completionHandler: (void (^)(NSError* error)) handler;
```



update the **google cloud messaging** *reg_id* for branch manager

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* regId = @"regId_example"; // Fresh GCM Reg_id

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUpdateGcmRegIdPostWithRegId:regId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUpdateGcmRegIdPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regId** | **NSString***| Fresh GCM Reg_id | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUpdateProfilePicturePost**
```objc
-(NSURLSessionTask*) technicianUpdateProfilePicturePostWithPicture: (NSURL*) picture
        completionHandler: (void (^)(NSError* error)) handler;
```



update the profile pic

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSURL* picture = [NSURL fileURLWithPath:@"/path/to/file.txt"]; // New Profile Picture. jpeg/jpg/png

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUpdateProfilePicturePostWithPicture:picture
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUpdateProfilePicturePost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picture** | **NSURL***| New Profile Picture. jpeg/jpg/png | 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUpdateTicketStatusPost**
```objc
-(NSURLSessionTask*) technicianUpdateTicketStatusPostWithTicketId: (NSNumber*) ticketId
    ticketType: (NSString*) ticketType
    status: (NSString*) status
        completionHandler: (void (^)(NSError* error)) handler;
```



Update `Ticket` Status

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSNumber* ticketId = @789; // 
NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSString* status = @"status_example"; // Otional filter by status (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUpdateTicketStatusPostWithTicketId:ticketId
              ticketType:ticketType
              status:status
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUpdateTicketStatusPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketId** | **NSNumber***|  | 
 **ticketType** | **NSString***| the type of the ticket in question | 
 **status** | **NSString***| Otional filter by status | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **technicianUploadReportPost**
```objc
-(NSURLSessionTask*) technicianUploadReportPostWithTicketType: (NSString*) ticketType
    file: (NSURL*) file
    ticketId: (NSNumber*) ticketId
    _description: (NSString*) _description
        completionHandler: (void (^)(NSError* error)) handler;
```



upload a report file

### Example 
```objc
SWGDefaultConfiguration *apiConfig = [SWGDefaultConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: TokenAuth)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"Authorization"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"Authorization"];


NSString* ticketType = @"ticketType_example"; // the type of the ticket in question
NSURL* file = [NSURL fileURLWithPath:@"/path/to/file.txt"]; // The jpeg file to be uploaded
NSNumber* ticketId = @789; // the `Ticket` id for which this report is uploaded
NSString* _description = @"_description_example"; // the details of the uploaded file (optional)

SWGTechnicianApi*apiInstance = [[SWGTechnicianApi alloc] init];

[apiInstance technicianUploadReportPostWithTicketType:ticketType
              file:file
              ticketId:ticketId
              _description:_description
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling SWGTechnicianApi->technicianUploadReportPost: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ticketType** | **NSString***| the type of the ticket in question | 
 **file** | **NSURL***| The jpeg file to be uploaded | 
 **ticketId** | **NSNumber***| the &#x60;Ticket&#x60; id for which this report is uploaded | 
 **_description** | **NSString***| the details of the uploaded file | [optional] 

### Return type

void (empty response body)

### Authorization

[TokenAuth](../README.md#TokenAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

