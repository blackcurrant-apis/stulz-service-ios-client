# SWGTechnicianShortProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**technicianId** | **NSNumber*** | UID for the technician | 
**technicianUserId** | **NSNumber*** | Foreign id to the USER Table | 
**technicianName** | **NSString*** | name of the Technician | 
**technicianRegionCode** | **NSArray&lt;NSString*&gt;*** | region codes to which the Technician is assigned | [optional] 
**technicianPhone** | **NSString*** | phone number of the Technician | 
**technicianAddress** | **NSString*** | home address of the Technician | [optional] 
**technicianPostCode** | **NSNumber*** | postal code for the address | [optional] 
**technicianProfilePicUrl** | **NSString*** | url to profile picture | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


