# SWGTechnicianStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ticketsInProgress** | **NSNumber*** |  | [optional] 
**amcTicketsNotResolved** | **NSNumber*** |  | [optional] 
**ticketsOnHold** | **NSNumber*** |  | [optional] 
**ticketsResolved** | **NSNumber*** |  | [optional] 
**ticketsUrgent** | **NSNumber*** |  | [optional] 
**ticketsOverdue** | **NSNumber*** |  | [optional] 
**pendingSpareApprovals** | **NSNumber*** |  | [optional] 
**pendingOvertimeApprovals** | **NSNumber*** |  | [optional] 
**ticketsAssignedToday** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


